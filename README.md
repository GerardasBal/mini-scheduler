# Mini scheduler

Application allow to create, update and delete TODO list entries.

![Untitled222](/uploads/a240562f949df4669b0990ae8c5e863e/Untitled222.png)

To run application on file hibernate.cfg.xml configuration is needed (password, username, database connection scheema).
Default configuration:

![Untitled22](/uploads/9cee522b291c9ebec975ec740060df0c/Untitled22.png)

To create new entry:
Need to fill 3 fields - select date, write time and subject (At the end press SAVE)

To update entry:
1. Select TODO line to update (click on line)
2. Edit data in data fields
3. Press Update

To delete entry:
1. Select TODO line (click on line)
2. Press Delete