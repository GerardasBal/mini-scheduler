import com.scheduler.dao.EventDAO;
import com.scheduler.model.Event;
import org.junit.Assert;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;




public class Test {

    @org.junit.Test
    public void insertNewTextWithDate() {
        try {
            String text = "Sample text";
            Event event = new Event(LocalDateTime.parse("2020-02-12 15:15"), text);
            EventDAO eventDAO = new EventDAO();
            eventDAO.insertEvent(event);
            Event eventFromDB = eventDAO.selectByID(1);
            Assert.assertEquals(event, eventFromDB);
        } catch (Exception e) {

        }


    }


}
