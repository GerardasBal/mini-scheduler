package com.scheduler.controller;

import com.scheduler.dao.EventDAO;
import com.scheduler.model.Event;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.util.Callback;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ViewController {


    @FXML
    private Button updateButton;

    @FXML
    private DatePicker dateEntry;

    @FXML
    private TextField timeEntry;

    @FXML
    private TextArea descriptionArea;

    @FXML
    private ListView<Event> listView;

    @FXML
    private void onSaveButtonClick() {
        String dateString = dateEntry.getValue().toString();
        String time = timeEntry.getText();
        String timeDate = dateString + " " + time;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime date = LocalDateTime.parse(timeDate, formatter);
        String subject = descriptionArea.getText();

        Event event = new Event(date, subject);
        EventDAO eventDAO = new EventDAO();
        eventDAO.insertEvent(event);
        System.out.println("Saved!");
        listViewUpdate();
        clearFields();

    }

    @FXML
    private void onUpdateButtonClick() {

        EventDAO eventDAO = new EventDAO();
        Event eventSelected = listView.getSelectionModel().getSelectedItem();
        if (eventSelected != null) {
            String dateTime = dateEntry.getValue() + timeEntry.getText();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-ddHH:mm");
            eventSelected.setDate(LocalDateTime.parse(dateTime, formatter));
            eventSelected.setSubject(descriptionArea.getText());
            eventSelected.setLastUpdate(new Date());
            eventDAO.updateEvent(eventSelected);
            System.out.println("Updated!");
            listViewUpdate();
        } else {
            System.out.println("No item selected!");
        }
        clearFields();
    }

    @FXML
    private void onDeleteButtonClick() {
        EventDAO eventDAO = new EventDAO();
        listView.refresh();
        Event eventSelected = listView.getSelectionModel().getSelectedItem();
        if (eventSelected != null) {
            eventDAO.deleteEvent(eventSelected);
            System.out.println("Delete!");
            listViewUpdate();
        } else {
            System.out.println("No item selected!");
        }
        clearFields();
    }

    private void listViewUpdate() {
        EventDAO eventDAO = new EventDAO();
        List<Event> events = eventDAO.findAllEventsWithCriteriaQuery();
        ObservableList<Event> list = FXCollections.observableArrayList();
        list.addAll(events);
        listView.setItems(list);
        listView.setCellFactory(param -> new ListCell<Event>() {
            @Override
            protected void updateItem(Event item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setText(null);
                    setStyle(null);
                } else {
                    setText(getItem().toString());
                    if(getItem().getDate().toLocalDate().isBefore(LocalDate.now())){
                        setStyle( "-fx-background-color: #fa4b4b;");
                    } else if (getItem().getDate().toLocalDate().isEqual(LocalDate.now())){
                        setStyle( "-fx-background-color: #ffff00;");
                    }
                }
            }
        });
        listView.refresh();
    }

    @FXML
    private void initialize() {
        listViewUpdate();
        dateEntry.setPromptText(LocalDate.now().toString());
        timeEntry.setPromptText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        System.out.println("Controller is initialized!");
    }

    private void clearFields() {
        dateEntry.getEditor().clear();
        timeEntry.clear();
        descriptionArea.clear();
        listView.getSelectionModel().clearSelection();
    }

    @FXML
    private void onSelectionUpdateFields() {
        Event eventSelected = listView.getSelectionModel().getSelectedItem();
        if (eventSelected != null) {
            dateEntry.getEditor().setText(eventSelected.getDate().toLocalDate().toString());
            timeEntry.setText(eventSelected.getDate().toLocalTime().toString());
            descriptionArea.setText(eventSelected.getSubject());
        }
    }


}
