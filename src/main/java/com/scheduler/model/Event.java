package com.scheduler.model;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(schema = "schedule_test", name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "event_id")
    private int eventId;

    @Column(name = "date", nullable = false)
    private LocalDateTime date;

    @Column(name = "subject", nullable = false)
    private String subject;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update", nullable = false)
    private Date lastUpdate;

    public Event() {
    }

    public Event(LocalDateTime date, String subject) {
        this.date = date;
        this.subject = subject;
        setLastUpdate(new Date());
    }

    public int eventId() {
        return eventId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date date) {
        this.lastUpdate = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event event = (Event) o;
        return date.equals(event.date) &&
                subject.equals(event.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, subject);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        sb.append(date.format(formatter));
        sb.append(" || ");
        sb.append(subject);

        return sb.toString();
    }
}
