package com.scheduler.dao;

import com.scheduler.model.Event;
import com.scheduler.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


public class EventDAO {

    public void insertEvent(Event event) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(event);
            transaction.commit();
            System.out.println("New Object inserted");
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void updateEvent(Event event) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(event);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    public Event selectByID(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Event event = session.find(Event.class, id);
        session.close();
        return event;
    }

    public void deleteEvent(Event event) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(event);
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Event> findAllEventsWithCriteriaQuery() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Event> cq = cb.createQuery(Event.class);
        Root<Event> rootEntry = cq.from(Event.class);
        CriteriaQuery<Event> all = cq.select(rootEntry);

        TypedQuery<Event> allQuery = session.createQuery(all);
        List<Event> results = allQuery.getResultList();
        session.close();
        return results;
    }

    public Event findEvent(Event eventToFind) {
        List<Event> events = findAllEventsWithCriteriaQuery();
        for (Event event : events) {
            if (event.equals(eventToFind)) {
                return event;
            }
        }
        return null;
    }

}

